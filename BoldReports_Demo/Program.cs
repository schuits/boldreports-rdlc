﻿using System;
using System.Data.SqlClient;
using System.IO;
using BoldReports.Web;
using BoldReports.Writer;
using Npgsql;

namespace BoldReports_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                //Register produc
                //Register Bold license
                Bold.Licensing.BoldLicenseProvider.RegisterLicense("glQvNEBBtEfWDrMRpohML7dtVIQuHtJzK6sQcFGeNmo=");

                //retrieve a set of data from Customers in NorthWind db
                //    Customers dsCustomers = GetDataSQL();
                Customers dsCustomers = GetDataPostGre();
                ReportDataSource datasource = new ReportDataSource("Customers", dsCustomers.Tables[0]);
                ReportDataSourceCollection dataSources = new ReportDataSourceCollection();
                dataSources.Add(datasource);

                //create a reportwriter instance 
                ReportWriter reportWriter = new ReportWriter();
                reportWriter.ReportProcessingMode = ProcessingMode.Local;
               
                //import the RDLC report
                FileStream inputStream = new FileStream("Report.rdlc", FileMode.Open, FileAccess.Read);
                reportWriter.LoadReport(inputStream);
                reportWriter.DataSources = dataSources;
                MemoryStream memoryStream = new MemoryStream();
                //WriterBase the RDLC report to a memorystream
                reportWriter.Save(memoryStream, WriterFormat.PDF);
               
                using (var fileStream = File.Create("report.pdf"))
                {
                    Console.WriteLine("Source length: {0}", memoryStream.Length.ToString());
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    memoryStream.CopyTo(fileStream, 100);
                    Console.WriteLine("Destination length: {0}", fileStream.Length.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static Customers GetDataSQL()
        {
            string constr = @"Data Source=localhost;Initial Catalog=Northwind;Integrated Security = true";
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT TOP 20 * FROM customers"))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (Customers dsCustomers = new Customers())
                        {
                            sda.Fill(dsCustomers, "DataTable1");
                            return dsCustomers;
                        }
                    }
                }
            }
        }
        private static Customers GetDataPostGre()
        {
            // PostgeSQL-style connection string
            #region super secret credentials
            string connstring = "Host=localhost;Username=postgres;Password=21hjox~!;Database=Northwind";
            #endregion
            // Making connection with Npgsql provider
            using (NpgsqlConnection conn = new NpgsqlConnection(connstring))
            { 
                string sql = "SELECT * FROM Customers fetch first 20 rows only";
                // data adapter making request from our connection
                using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn))
                {
                    using (Customers dsCustomers = new Customers())
                    {
                        da.Fill(dsCustomers, "DataTable1");
                        return dsCustomers;
                    }
                }
            }
        }
    }
}
